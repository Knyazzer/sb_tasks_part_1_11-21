#include "Helpers.h"
#include <cmath>
#include <iostream> 
using namespace std;

int degreeSum(int a, int b, int e) {
	return pow(a + b, e);
}

int Helpers::anyDegreeSum(int a, int b, int e) {
	return degreeSum(a, b, e);
}

int squareSum(int a, int b) {
	return degreeSum(a, b, 2);

}

void printOddEvenNumbers(int EvenOdd, int lenght) {
	bool flagEvenOdd = EvenOdd % 2 == 0;

	for (int i = 0; i <= lenght; i++) {
		if (i % 2 == 0 && flagEvenOdd) {
			cout << i << " ";
		}
		if (i % 2 != 0 && !flagEvenOdd) {
			cout << i << " ";
		}
	}
	cout << endl;
}