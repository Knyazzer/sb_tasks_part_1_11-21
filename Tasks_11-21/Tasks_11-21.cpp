﻿// Tasks_11-21.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Helpers.h"
#include <iostream>

using namespace std;

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void SetVector(double _x, double _y, double _z)
	{
		x = _x; y = _y; z = _z;
	}
	void GetVector()
	{
		cout << "\n" << x << " " << y << " " << z << endl;
	}
	double GetLenghtVector() {
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

private:
	double x;
	double y;
	double z;
};


int main()
{
	Vector H;
	H.GetVector();
	Vector M(1, 4, 6);
	M.GetVector();
	M.SetVector(3, 4.5, 7.1);
	M.GetVector();
	cout<<M.GetLenghtVector();

}

